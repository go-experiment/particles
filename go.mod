module particles

go 1.15

require (
	github.com/VividCortex/multitick v0.0.0-20201215174257-c8fd171df444
	github.com/gen2brain/raylib-go v0.0.0-20210206114709-72e19e9bd197
	github.com/go-gl/gl v0.0.0-20190320180904-bf2b1f2f34d7
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20201108214237-06ea97f0c265
	github.com/marcusolsson/tui-go v0.4.0
	github.com/prometheus/procfs v0.6.0
)
