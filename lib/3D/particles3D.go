package particle3D

import (
	"github.com/VividCortex/multitick"
	"math"
	"time"
)

type Point struct {
	X, Y, Z float32
}

type BoundingBox struct {
	Center               Point
	Height, Width, Depth float32
}

type Direction struct {
	Theta float32
	Phi   float32
}

type Motion struct {
	Velocity     float32   // unit per tic
	Acceleration float32   // unit per tic^2
	Direction    Direction // theta and phi for spehrical coordinates
}

type Particle struct {
	Location Point
	Bound    *BoundingBox
	Motion   Motion
}

type Particles struct {
	Group []Particle
	tick  *multitick.Ticker
	bound *BoundingBox
}

func (p *Particle) TickTock(c <-chan time.Time) {
	for _ = range c {

		p.Motion.Velocity += p.Motion.Acceleration

		/*
			x = r * sinϕ * cosθ
			y = r * sinϕ * sinθ
			z = r * cosϕ
		*/

		cosPhi := float32(math.Cos(float64(p.Motion.Direction.Phi)))
		sinPhi := float32(math.Sin(float64(p.Motion.Direction.Phi)))

		cosTheta := float32(math.Cos(float64(p.Motion.Direction.Theta)))
		sinTheta := float32(math.Sin(float64(p.Motion.Direction.Theta)))

		var x float32 = p.Motion.Velocity * sinTheta * cosPhi
		var z float32 = p.Motion.Velocity * sinPhi * sinTheta
		var y float32 = p.Motion.Velocity * cosTheta

		nX := p.Location.X + float32(x)
		nZ := p.Location.Z + float32(z)
		nY := p.Location.Y + float32(y)

		//side walls detect
		if p.Location.X < (p.Bound.Center.X+(p.Bound.Width/2)) && p.Location.X > (p.Bound.Center.X-(p.Bound.Width/2)) {
			p.Location.X = nX
		} else {

			if (nX - p.Bound.Center.X + (p.Bound.Width / 2)) > (p.Bound.Center.X - (p.Bound.Width / 2) - nX) {
				p.Location.X = p.Bound.Center.X + (p.Bound.Width / 2) - 1
			} else {
				p.Location.X = p.Bound.Center.X - (p.Bound.Width / 2) + 1
			}

			p.Motion.Direction.Theta = degToRad(0) - p.Motion.Direction.Theta
			p.Motion.Direction.Phi = (0 - 1) * p.Motion.Direction.Phi
		}

		//top / right wall detect
		if p.Location.Y < p.Bound.Center.Y+(p.Bound.Height/2) && p.Location.Y > p.Bound.Center.Y-(p.Bound.Height/2) {
			p.Location.Y = nY
		} else {
			if (nY - p.Bound.Center.Y + (p.Bound.Height / 2)) > (p.Bound.Center.Y - (p.Bound.Height / 2) - nY) {
				p.Location.Y = p.Bound.Center.Y + (p.Bound.Height / 2) - 1

			} else {
				p.Location.Y = p.Bound.Center.Y - (p.Bound.Height / 2) + 1
			}

			p.Motion.Direction.Theta = degToRad(180) - p.Motion.Direction.Theta
		}

		//front rear / depth wall detect
		if p.Location.Z <= p.Bound.Center.Z+(p.Bound.Depth/2) && p.Location.Z >= p.Bound.Center.Z-(p.Bound.Depth/2) {
			p.Location.Z = nZ
		} else {
			if (nZ - p.Bound.Center.Z + (p.Bound.Depth / 2)) > (p.Bound.Center.Z - (p.Bound.Depth / 2) - nZ) {
				p.Location.Z = p.Bound.Center.Z + (p.Bound.Depth / 2) - 1

			} else {
				p.Location.Z = p.Bound.Center.Z - (p.Bound.Depth / 2) + 1

			}
			p.Motion.Direction.Phi = (0 - 1) * p.Motion.Direction.Phi
		}
	}
}

func NewParticles(bound *BoundingBox) Particles {
	var p Particles = Particles{
		Group: []Particle{},
		tick:  multitick.NewTicker(10*time.Millisecond, 1*time.Second),
		bound: bound,
	}
	return p
}

func (ps *Particles) AddParticle(x float32, y float32, z float32, v float32, theta float32, phi float32) {

	var p = Particle{
		Location: Point{X: x, Y: y},
		Motion: Motion{
			Velocity:     v,
			Acceleration: 0,
			Direction: Direction{
				Theta: degToRad(theta),
				Phi:   degToRad(phi),
			},
		},
		Bound: ps.bound,
	}
	ps.Group = append(ps.Group, p)
}

func (ps *Particles) StartTicking() {
	for i := range ps.Group {
		go ps.Group[i].TickTock(ps.tick.Subscribe())
	}
}

func degToRad(deg float32) float32 {
	return (deg * math.Pi / 180)
}

func radToDeg(rad float32) float32 {
	return (rad * 180 / math.Pi)
}
