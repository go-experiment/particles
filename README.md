# Particles

learning Go by experimenting with
 - particles
 - raylib
 - go routines
 - github.com/VividCortex/multitick

## Requirements

The following needs to ge installed before we get started
```bash
sudo apt-get install libgl1-mesa-dev libxcursor-dev libxrandr-dev libxinerama-dev libxi-dev libxxf86vm-dev
```

## Getting started

### Make

make can be used

```bash
# Build an executable named particles
make build

# Build an executable named particles and run it
make run

# Clean eveyrything up
make clean
```

### Go commands

```bash
# Run without making an executable
go run .

# build an executable
go build -o <name of executable> main.go
```

### Configurables

#### Amount of particles

```bash
<run command> -p <number of particles>
```
