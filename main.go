package main

import (
	"flag"
	"fmt"
	"github.com/gen2brain/raylib-go/raylib"
	"math/rand"
	p3d "particles/lib/3D"
	"time"
)

const (
	screenWidth  = 1024
	screenHeight = 720
	centerX      = 0
	centerY      = 0
	centerZ      = 0
)

var bound = p3d.BoundingBox{
	Center: p3d.Point{X: 0, Y: 0, Z: 0},
	Width:  170,
	Height: 170,
	Depth:  170,
}

var ps = p3d.NewParticles(&bound)

func main() {

	number := flag.Int("n", 5, "number of random particles")
	flag.Parse()
	fmt.Printf("Adding %v random particles\n", *number)

	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)

	for i := 0; i < *number; i++ {
		fmt.Println(i)
		ps.AddParticle(
			centerX,
			centerY,
			centerZ,
			0.5,
			float32(r1.Intn(180)),
			float32(r1.Intn(180)),
		)

	}

	ps.StartTicking()
	openGl3d()

	select {}
}

func openGl3d() {
	rl.InitWindow(screenWidth, screenHeight, "3d Particles")

	camera := rl.Camera3D{}

	camera.Position = rl.NewVector3(300.0, 300.0, 300.0)
	camera.Target = rl.NewVector3(0.0, 0.0, 0.0)
	camera.Up = rl.NewVector3(0.0, 1.0, 0.0)
	camera.Fovy = 45.0
	camera.Type = rl.CameraPerspective

	cubePosition := rl.NewVector3(
		float32(bound.Center.X),
		float32(bound.Center.Y),
		float32(bound.Center.Z),
	)

	rl.SetCameraMode(camera, rl.CameraThirdPerson)

	rl.SetTargetFPS(60)

	for !rl.WindowShouldClose() {
		rl.UpdateCamera(&camera) // Update camera

		rl.BeginDrawing()

		rl.ClearBackground(rl.RayWhite)

		rl.BeginMode3D(camera)

		for _, p := range ps.Group {
			rl.DrawSphere(
				rl.Vector3{
					X: p.Location.X,
					Y: p.Location.Y,
					Z: p.Location.Z,
				},
				10,
				rl.Fade(rl.Blue, 0.9))
		}

		rl.DrawCubeWires(
			cubePosition,
			float32(bound.Width),
			float32(bound.Height),
			float32(bound.Depth),
			rl.Gray,
		)

		rl.DrawCube(
			cubePosition,
			float32(bound.Width),
			float32(bound.Height),
			float32(bound.Depth),
			rl.Fade(rl.Gray, 0.1),
		)

		//rl.DrawGrid(20, 10.0)
		rl.EndMode3D()
		rl.EndDrawing()
	}

	rl.CloseWindow()

}
